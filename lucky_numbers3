# using Sage Math (a type of Python), specifically "Sage Cell Server"

"""links (roughly in order of importance/usage):
https://sagecell.sagemath.org/
oeis.org
https://alteredqualia.com/visualization/hn/sequence/
https://www.wolframalpha.com/
"""

import heapq

class LuckyStruct:
    n = 0
    a = 0
    b = 0
    c = 0
    
    def increment(self):
        self.n += 1
        
    def evaluate(self):
        return self.a*self.n*self.n + self.b*self.n + self.c

    def __init__(self, n, a, b, c):
        self.n = n
        self.a = a
        self.b = b
        self.c = c
        
    def __lt__(self, other):
        return False

def count_factors (factorization):
    sum = 0
    for factor in factorization:
        sum += factor[1]
    return sum

# example ... 44, 1, 112 ... where lower=44, upper=112, a = 77 = (44+112)/2 - 1, b = 34 = (112-44)/2, c = 1
def resolve_quadratic_coefficients(lower, upper):
    difference = upper - lower
    mean = lower + (upper - lower)/2
    a = mean - 1
    b = difference/2
    c = 1
    return [a, b, c]

def polygonal (sides, offset):
    # https://en.wikipedia.org/wiki/Polygonal_number#Formula
    a = sides/2 - 1
    b = -sides/2 + 2
    c = offset
    return (a, b, c)
    
def lucky_sieve(euler_prediction, sequence_priority_queue, array_size):
    while len(sequence_priority_queue) > 0:
        top = heapq.heappop(sequence_priority_queue)
        if top[0] >= array_size:
            break
        euler_prediction[top[0]] += 1 # there's another prime factor (predicted)
        top[1].increment()
        next_index = top[1].evaluate()
        heapq.heappush(sequence_priority_queue, (next_index, top[1]))
        
def add_sequence(sequence_priority_queue, a, b, c, array_size):
    sequence = LuckyStruct(0, a, b, c)
    next_index = sequence.evaluate()
    if next_index < array_size:
        heapq.heappush(sequence_priority_queue, (next_index, sequence))
        
    sequence = LuckyStruct(1, a, -b, c)
    next_index = sequence.evaluate()
    if next_index < array_size:
        heapq.heappush(sequence_priority_queue, (next_index, sequence))

def add_polygonal(sequence_priority_queue, sides, euler_number, row_offset, array_size):
    (a, b, c) = polygonal(sides, euler_number*row_offset - 1)
    add_sequence(sequence_priority_queue, a, b, c, array_size)
    
def create_euler_definition(array_size, euler_number):
    euler_definition = [0] * array_size
    for i in range(0, array_size):
        euler_definition[i] = count_factors(factor(i^2 + i + euler_number))
    return euler_definition

def create_heegner_definition(array_size, heegner_number):
    heegner_definition = [0] * array_size
    for i in range(0, array_size):
        heegner_definition[i] = count_factors(factor(4*i^2 + heegner_number))
    return heegner_definition

def create_euler_prediction(array_size, euler_number):
    euler_prediction = [1] * array_size
    
    sequence_priority_queue = []
    
    # euler_number = 41
    heapq.heappush(sequence_priority_queue, (1*euler_number-1, LuckyStruct(0, 1/1, 0/1, 1*euler_number-1))) # square numbers need to be computed separately

    for n in range(6, 19, 2):
        add_polygonal(sequence_priority_queue, n, 41, (n-2)/2, array_size) # first & second n-gonal numbers from hexagonal (6-gonal) to 18-gonal.
    heapq.heappush(sequence_priority_queue, (9*euler_number-1, LuckyStruct(0, 9,  -8, 9*euler_number-1))) 
    heapq.heappush(sequence_priority_queue, (9*euler_number+16, LuckyStruct(2, 9,  +8, 9*euler_number-1))) # I think you might just have to selectively ignore some elements in the sequence on the negative side
    
    # + 6 x2 + 1 x + 1 
    add_sequence(sequence_priority_queue, 6, 1, 6*euler_number-2, array_size)
    
    #2+ 2+ 2  1  1  2  2  1  1  1  2  1  2  2  1  2  1  2+ 1  1  2+ 2  1  1  1  1  1  2  1  1  1  2  1  2  2  1  1  1  2  2+ 2  
    # 0  1                                               17       20                                                       39 
    lucky_sieve(euler_prediction, sequence_priority_queue, array_size)
    return euler_prediction

def create_heegner_prediction(array_size, euler_number): # euler_number is not a typo because what matters is the number of columns (which is the euler_number)
    heegner_prediction = [1] * array_size
    
    sequence_priority_queue = []
    heapq.heappush(sequence_priority_queue, (floor(euler_number/2), LuckyStruct(0, 1/2, 1/2, floor(euler_number/2)))) # triangular numbers need to be computed separately

    lucky_sieve(heegner_prediction, sequence_priority_queue, array_size)
    return heegner_prediction

def test_comparison(correct_answer, predicted_answer):
    if correct_answer == predicted_answer:
        return " " # correct, no need to clutter data visualization
    elif predicted_answer > correct_answer:
        if correct_answer > 1:
            return chr(ord('A') + (predicted_answer-correct_answer) - 1)
        return "-" # false negative, which isn't supposed to happen (programmer error in all known cases so far)
    else: # predicted_answer < correct_answer:
        if predicted_answer > 1:
            return chr(ord('a') + (correct_answer-predicted_answer) - 1)
        return "+" # false positive, which indicates a sequence hasn't been included in the algorithm yet

def sequence_comparison(definition_array, comparison_array):
    result = [0] * len(definition_array)
    for index in range(0, len(definition_array)):
        result[index] = test_comparison(definition_array[index], comparison_array[index])
    return result

def print_error(error_array, columns):
    first_discrepancy = error_array.index('+')
    print("%3.2f" % (first_discrepancy/columns), end=": ")
    for index in range(first_discrepancy, len(error_array)):
        if error_array[index] == "+":
            print("%3d" % (index - first_discrepancy + 1), end=" ")
    print()

def print_comparison(definition_array, error_array, rows, columns, offset):
    for y in range(0, rows):
        for x in range(0, columns):
            index = y*columns + x + offset
            correct_answer = definition_array[index]
            answer_error = error_array[index]
            print(correct_answer, end="")
            print(answer_error, end=" ")
        print()
        
def print_euler(definition_array, error_array, rows, columns):
    print("   " * 1, end="")
    print_comparison(definition_array, error_array, 1, columns-1, 0)
    print_comparison(definition_array, error_array, rows-1, columns, columns - 1)
    print()
    
def print_heegner(definition_array, error_array, rows, columns):
    print("   " * ceil(columns/2), end="")
    print_comparison(definition_array, error_array, 1, floor(columns/2), 0)
    print_comparison(definition_array, error_array, rows-1, columns, floor(columns/2))
    print()
    
rows = 200

n = [3, 5, 11, 17, 41]
array_size = rows*max(n)
euler_definition = [0] * len(n)
euler_prediction = [0] * len(n)
euler_error = [0] * len(n)

for index in range(0, len(n)):
    euler_definition[index] = create_euler_definition(array_size, n[index])
    euler_prediction[index] = create_euler_prediction(array_size, n[index])
    euler_error[index] = sequence_comparison(euler_definition[index], euler_prediction[index])
    #print_error(euler_error[index], n[index])
    
print()
n = [3, 5, 11, 17, 41]
array_size = rows*max(n)
heegner_definition = [0] * len(n)
heegner_prediction = [0] * len(n)
heegner_error = [0] * len(n)

for index in range(0, len(n)):
    heegner_definition[index] = create_heegner_definition(array_size, 4*n[index]-1)
    heegner_prediction[index] = create_heegner_prediction(array_size, n[index])
    heegner_error[index] = sequence_comparison(heegner_definition[index], heegner_prediction[index])
    #print_error(heegner_error[index], n[index])

print()

print_index = 4
print_euler(euler_definition[print_index], euler_error[print_index], rows, n[print_index])
print_heegner(heegner_definition[print_index], heegner_error[print_index], rows, n[print_index])
